<?php 
session_start();
include_once 'config/init.php'; 

if(!isLoggedIn()){
	header('Location: index.php');
}

/**
 * Created by IntelliJ IDEA.
 * User: Nelson Willian - nelson.ws@outlook.com
 * Date: 28/12/2017
 * Time: 17:36
 * Use Front-End: KeenThemes Version: 4.7.5
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
		<?php include_once 'head.php'; ?>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-footer-fixed">
        <div class="page-wrapper">
      		<?php include_once 'header.php'; ?>
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
								<?php include_once 'sidebar-menu.php'; ?>
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Blank Page</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Page Layouts</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#"><i class="icon-bell"></i> Action</a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="icon-shield"></i> Another action</a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="icon-user"></i> Something else here</a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="#"><i class="icon-bag"></i> Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Blank Page Layout
                            <small>blank page layout</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="note note-info">
                            <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
								 <?php include_once 'quick-sidebar-menu.php'; ?>

            </div>
            <!-- END CONTAINER -->
						<?php include_once 'footer.php'; ?>

						<?php include_once 'menu-flutuante.php'; ?>

        </div>
        				<?php include_once 'js-footer.php'; ?>
    </body>
</html>